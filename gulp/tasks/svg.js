module.exports = function() {

    $.gulp.task('svg', function() {
        return $.gulp.src('src/static/svg/*.svg')
          .pipe($.gp.svgSprite({
              mode: {
                  symbol: {
                      sprite: "../sprite.svg",
                  }
              }
          }))
          .pipe($.gulp.dest('build/img/svg/'));
    });
}
