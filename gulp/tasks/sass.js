module.exports = function() {
    $.gulp.task('sass', function(){
        return $.gulp.src('src/static/scss/main.scss')
            .pipe($.gp.sourcemaps.init())
            .pipe($.gp.autoprefixer())
            .on("error", $.gp.notify.onError({
                message: "Error: <%= error.message %>",
                title: "style"
            }))
            // Минифицированная версия
            .pipe($.gp.sass({outputStyle: 'compressed'}))
            .pipe($.gp.sourcemaps.write('./'))
            .pipe($.gulp.dest('build/css/'))

            .pipe($.bs.reload({
                stream:true
            }));
    });
}
