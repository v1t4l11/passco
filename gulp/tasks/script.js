module.exports = function() {
    // Обработка файлов библиотек и перенос в build в исходном виде
    // Перенос файла common.js в папку build
    $.gulp.task('scripts', function() {
        return $.gulp.src('src/static/js/*.js')
        .pipe($.gulp.dest('build/js/'))
        .pipe($.bs.reload({
            stream: true
        }));
    });

};
